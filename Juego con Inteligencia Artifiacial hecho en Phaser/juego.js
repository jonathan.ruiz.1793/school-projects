 // import * as synaptic from "synaptic";
// import * as Phaser from "phaser";
var w = 800;
var h = 400;
var game = new Phaser.Game(w, h, Phaser.CANVAS, "phaser-game", {
  preload,
  create,
  update,
});
function preload() {
  game.load.image("background", "assets/game/kof.png");
  game.load.spritesheet("player", "assets/sprites/iori.png", 183, 138, 4);
  game.load.spritesheet("enemy", "assets/sprites/ryu.png", 42, 100, 3);
  game.load.image("bullet", "assets/sprites/purple_ball.png");
  game.load.image("menu", "assets/game/menu2.png");

}

var player;
var bg;

var bullet;
var bullet_fired = false;

var jumpButton;
var downButton;

var menu;

var bullet_speed;
var bullet_displacement;
var bullet_height = h;
var stay_on_air;
var stay_on_floor;
var stay_down;

var nn_network;
var nn_trainer;
var nn_output;
var trainingData = [];

var auto_mode = false;
var training_complete = false;


function create() {
  game.physics.startSystem(Phaser.Physics.ARCADE);
  game.physics.arcade.gravity.y = 700;
  game.time.desiredFps = 60;

  bg = game.add.tileSprite(0, 0, w, h, "background");
  enemy = game.add.sprite(w - 100, h - 70, "enemy");
  bullet = game.add.sprite(w - 100, h - 60, "bullet");
  player = game.add.sprite(50, h, "player");


  game.physics.enable(player);
  player.body.collideWorldBounds = true;
  var run = player.animations.add("run", [1, 2, 3, 4, 5, 6, 7]);
  player.animations.play("run", 12, true);

  game.physics.enable(enemy);
  enemy.body.collideWorldBounds = true;
  var buu = enemy.animations.add("buu");
  enemy.animations.play("buu", 10, true);

  game.physics.enable(bullet);
  bullet.body.collideWorldBounds = true;
  bullet.body.allowGravity = false;
  pause_label = game.add.text(w / 2, 20, "Pausa", {
    font: "20px Arial",
    fill: "#fff"
  });
  pause_label.inputEnabled = true;
  pause_label.events.onInputUp.add(pause, self);
  game.input.onDown.add(un_pause, self);

  jumpButton = game.input.keyboard.addKey(Phaser.Keyboard.UP);
  downButton = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);

  nn_network = new synaptic.Architect.Perceptron(3, 40, 3);
  nn_trainer = new synaptic.Trainer(nn_network);

}

function train_nn() {//sacado de codigo de profe
  nn_trainer.train(trainingData, {
    rate: 0.0003,
    iterations: 10000,
    shuffle: true
  });
}

function get_op_from_trainedData(input_param) {
  nn_output = nn_network.activate(input_param);
  var on_air = Math.round(nn_output[0] * 100);
  var on_floor = Math.round(nn_output[1] * 100);
  var on_down = Math.round(nn_output[2] * 100);
  console.log(
    "Probabilidades de: ",
    "Saltar %: " +
      on_air +
      " | Nada %: " +
      on_floor +
      " | Agacharse %: " +
      on_down
  );
  return [
    nn_output[0] >= nn_output[1] && nn_output[0] >= nn_output[2],
    nn_output[2] >= nn_output[1]
  ];
}

function pause() {
  game.paused = true;
  menu = game.add.sprite(w / 2, h / 2, "menu");
  menu.anchor.setTo(0.5, 0.5);
}

function un_pause(event) {
  if (game.paused) {
    var menu_x1 = w / 2 - 270 / 2,
      menu_x2 = w / 2 + 270 / 2,
      menu_y1 = h / 2 - 180 / 2,
      menu_y2 = h / 2 + 180 / 2;

    var mouse_x = event.x,
      mouse_y = event.y;

    if (
      mouse_x > menu_x1 &&
      mouse_x < menu_x2 &&
      mouse_y > menu_y1 &&
      mouse_y < menu_y2
    ) {
      if (
        mouse_x >= menu_x1 &&
        mouse_x <= menu_x2 &&
        mouse_y >= menu_y1 &&
        mouse_y <= menu_y1 + 90
      ) {
        training_complete = false;
        trainingData = [];
        auto_mode = false;
      } else if (
        mouse_x >= menu_x1 &&
        mouse_x <= menu_x2 &&
        mouse_y >= menu_y1 + 90 &&
        mouse_y <= menu_y2
      ) {
        if (!training_complete) {
          console.log(
            "",
            "Aprendiendo a jugar solo con  " +
              trainingData.length +
              " entradas de aprendizaje"
          );
          train_nn();
          training_complete = true;
        }
        auto_mode = true;
      }

      menu.destroy();
      reset_state_variables();
      game.paused = false;
    }
  }
}

function reset_state_variables() {
  player.body.velocity.x = 0;
  player.body.velocity.y = 0;
  bullet.body.velocity.x = 0;

  bullet.position.x = w - 100;

  if (Math.floor(Math.random() * 2 + 1) == 1) {
    bullet.position.y = h - 85;
    bullet_height = h;
  } else {
    bullet.position.y = h;
    bullet_height = 0;
  }

  player.position.x = 50;

  bullet_fired = false;
}

function jump() {

  player.body.velocity.y = -270;
}

function down() {
  player.animations.play("down", 12, true);
  player.scale.setTo(0.4, 0.3);
  player.body.velocity.y = 5000;
}

function update() {
  bg.tilePosition.x -= 1; //moving background

  game.physics.arcade.collide(bullet, player, collisionHandler, null, this);

  stay_on_floor = 1;
  stay_on_air = 0;
  stay_down = 0;

  if (!player.body.onFloor()) {
    stay_on_floor = 0;
    stay_on_air = 1;
  }

  if (downButton.isDown && player.body.onFloor()) {
    stay_down = 1;
    stay_on_floor = 0;
    stay_on_air = 0;
  }

  bullet_displacement = Math.floor(player.position.x - bullet.position.x);

  if (auto_mode == false && jumpButton.isDown && player.body.onFloor()) {
    jump();
  }

  if (auto_mode == false && downButton.isDown && player.body.onFloor()) {
    down();
  }

  if (auto_mode == false && !downButton.isDown && player.body.onFloor()) {
    player.animations.play("run", 12, true);
    player.scale.setTo(0.6, 0.6);
  }

  if (auto_mode == true && !downButton.isDown && player.body.onFloor()) {
    player.animations.play("run", 12, true);
    player.scale.setTo(0.6, 0.6);
  }
  if (auto_mode == true && bullet.position.x > 0 && player.body.onFloor()) {
    if (
      get_op_from_trainedData([
        bullet_displacement,
        bullet_speed,
        bullet_height
      ])[0]
    ) {
      jump();
    }

    if (
      get_op_from_trainedData([
        bullet_displacement,
        bullet_speed,
        bullet_height
      ])[1]
    ) {
      down();
    }
  }

  if (bullet_fired == false) {
    fire();
  }

  if (bullet.position.x <= 0) {
    reset_state_variables();
  }

  if (auto_mode == false && bullet.position.x > 0) {
    trainingData.push({
      input: [bullet_displacement, bullet_speed, bullet_height],
      output: [stay_on_air, stay_on_floor, stay_down]
    });
    console.log(
      "Desplazamiento Bala " +
        bullet_displacement +
        ", Velocidad Bala: " +
        bullet_speed +
        ", Altura de Bala: " +
        bullet_height +
        "| Up: " +
        stay_on_air +
        " | Normal: " +
        stay_on_floor +
        " | Down:" +
        stay_down +
        " |"
    );
  }
}

function fire() {
  bullet_speed = -1 * getRandomSpeed(300, 800);
  bullet.body.velocity.x = bullet_speed;

  var random_boolean = Math.random() >= 0.5;
  bullet_fired = true;
}

function collisionHandler() {
  pause();
}

function getRandomSpeed(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

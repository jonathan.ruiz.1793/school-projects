function make2DArray(cols, rows) {//creates a function to create a two dimension array
    let arr = new Array(cols); //variable to set a new array of columns to arr
    for (let i = 0; i < arr.length; i++){//for to create the rows of the 2D array
        arr[i] = new Array(rows);//creates the 2D array by inserting rows into columns
    }
    return arr;//return the array
}
//declaracion de variables
let grid;
let cols;
let rows;
let resolution = 10;

function setup() {// crea el canvas del explorador
    createCanvas(600, 400);
    cols = width / resolution; //implementa los valores para las variables de columnas y filas
    rows = height / resolution;

    grid = make2DArray(cols, rows);// al arreglo final se le nombra grid
    for (let i =0; i<cols; i++){// for para llenado de matriz con numeros del 0 al 1
        for (let j = 0; j < rows; j++){
            grid[i][j] = Math.floor(random(2));
        }
    }
}
function draw() {
    background(0)//pone el el fondo color negro

    for (let i =0; i<cols; i++){//ciclo para pintar los lugares en los que se tenga el valor 1 con blanco
        for (let j = 0; j < rows; j++){
            let x = i * resolution;
            let y = j * resolution;
            if(grid[i][j] == 1){// si es uno se pinta blanco
                fill(255);
                stroke(0);//pinta de negro los bordes
                rect(x, y, resolution-1, resolution-1); //crear un margen de la posicion del arreglo en donde haya 1s
            }

        }
    }
    let next = make2DArray(cols, rows);//array for next generation in GOL
        //compute next based on grid

    for (let i =0; i<cols; i++){
        for (let j = 0; j < rows; j++){


            //count live neighbors
            let neighbors = countNeighbors(grid, i , j);

            let state = grid[i][j];// declara varible state que sirve para ver el estado de la celula actual

            if (state == 0 && neighbors == 3){//checa si una celular muerta puede revivir en NexGen
                next[i][j] = 1;
            }else if (state ==1 && (neighbors < 2 || neighbors > 3)){//checa si una celula viva va a morir en NextGen
                next[i][j] = 0;
            }else{
                next[i][j] = state;//si no aplica en esa posicion ninguna de las anteriores la siguiente genreacion
            }                       //en esa posicion sera igual a la generacion anterior

        }
    }
    grid = next; //Esto asigna la generacion nueva para que sea la nueva generacion vieja, en la proxima iteracion
}


function countNeighbors(grid, x, y) {
    // (x + y + column) module of columns :: this is the formula to remove the edges
    let sum = 0;
    for (let i = -1; i<2; i++){//estos fors son para revisar a los vecinos dado un punto x, y
        for (let j = -1; j<2; j++){//como los vecinos no pueden estar alejados mas de una posicion por eso va del
                                    //numero -1 a 1
            let col = (x + i + cols) % cols; //se da la posicion de la columna del vecino
            let row = (y + j + rows) % rows;//se da la posicion de la fila del vecino
            sum += grid[col][row]; // en el grid se da la posicion en la matriz y se guarda esa posicion en sum
        }                          // y tener que agregar cada posicion adyacente al punto a evaluar
    }
    sum -= grid[x][y];//remueve de la suma el punto x, y que seria la posicion dentro del grid a evaluar
    return sum;     //regresa sum con la posicion de todos los vecinos
}
